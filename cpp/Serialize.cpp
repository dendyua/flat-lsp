
#include "Serialize.hpp"

#include "Debug.hpp"
#include "Types.hpp"




namespace Flat {
namespace Lsp {




template <typename T>
struct ParseValue {
	static T go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kObjectType) {
			throw ParseError{};
		}

		return fromJson<T>(v);
	}
};




template <typename T>
struct ParseValue<std::vector<T>> {
	static std::vector<T> go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kArrayType) {
			throw ParseError{};
		}

		std::vector<T> vector;
		for (auto & val : v.GetArray()) {
			vector.push_back(ParseValue<T>::go(val));
		}
		return vector;
	}
};


template <>
struct ParseValue<std::nullptr_t> {
	static std::nullptr_t go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kNullType) {
			throw ParseError{};
		}

		return nullptr;
	}
};


template <>
struct ParseValue<bool> {
	static bool go(const rapidjson::Value & v)
	{
		switch (v.GetType()) {
		case rapidjson::kFalseType: return false;
		case rapidjson::kTrueType:  return true;
		default: throw ParseError{};
		}
	}
};


template <>
struct ParseValue<int> {
	static int go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kNumberType) {
			throw ParseError{};
		}

		return v.GetInt();
	}
};


template <>
struct ParseValue<std::string> {
	static std::string go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kStringType) {
			throw ParseError{};
		}

		return std::string(v.GetString(), v.GetStringLength());
	}
};


template <>
struct ParseValue<std::any> {
	static std::any go(const rapidjson::Value &)
	{
		return {};
	}
};


template <>
struct ParseValue<CompletionItemTag> {
	static CompletionItemTag go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kNumberType) {
			throw ParseError{};
		}

		const auto i = static_cast<CompletionItemTag>(v.GetInt());

		switch (i) {
		case CompletionItemTag::Deprecated:
			return i;
		}

		throw ParseError{};
	}
};


template <>
struct ParseValue<SymbolKind> {
	static SymbolKind go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kNumberType) {
			throw ParseError{};
		}

		const auto i = static_cast<SymbolKind>(v.GetInt());

		switch (i) {
		case SymbolKind::File:
		case SymbolKind::Module:
		case SymbolKind::Namespace:
		case SymbolKind::Package:
		case SymbolKind::Class:
		case SymbolKind::Method:
		case SymbolKind::Property:
		case SymbolKind::Field:
		case SymbolKind::Constructor:
		case SymbolKind::Enum:
		case SymbolKind::Interface:
		case SymbolKind::Function:
		case SymbolKind::Variable:
		case SymbolKind::Constant:
		case SymbolKind::String:
		case SymbolKind::Number:
		case SymbolKind::Boolean:
		case SymbolKind::Array:
		case SymbolKind::Object:
		case SymbolKind::Key:
		case SymbolKind::Null:
		case SymbolKind::EnumMember:
		case SymbolKind::Struct:
		case SymbolKind::Event:
		case SymbolKind::Operator:
		case SymbolKind::TypeParameter:
			return i;
		}

		throw ParseError{};
	}
};


template <>
struct ParseValue<CompletionItemKind> {
	static CompletionItemKind go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kNumberType) {
			throw ParseError{};
		}

		const auto i = static_cast<CompletionItemKind>(v.GetInt());

		switch (i) {
		case CompletionItemKind::Text:
		case CompletionItemKind::Method:
		case CompletionItemKind::Function:
		case CompletionItemKind::Constructor:
		case CompletionItemKind::Field:
		case CompletionItemKind::Variable:
		case CompletionItemKind::Class:
		case CompletionItemKind::Interface:
		case CompletionItemKind::Module:
		case CompletionItemKind::Property:
		case CompletionItemKind::Unit:
		case CompletionItemKind::Value:
		case CompletionItemKind::Enum:
		case CompletionItemKind::Keyword:
		case CompletionItemKind::Snippet:
		case CompletionItemKind::Color:
		case CompletionItemKind::File:
		case CompletionItemKind::Reference:
		case CompletionItemKind::Folder:
		case CompletionItemKind::EnumMember:
		case CompletionItemKind::Constant:
		case CompletionItemKind::Struct:
		case CompletionItemKind::Event:
		case CompletionItemKind::Operator:
		case CompletionItemKind::TypeParameter:
			return i;
		}

		throw ParseError{};
	}
};


template <>
struct ParseValue<DiagnosticTag> {
	static DiagnosticTag go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kNumberType) {
			throw ParseError{};
		}

		const auto i = static_cast<DiagnosticTag>(v.GetInt());

		switch (i) {
		case DiagnosticTag::Unnecessary:
		case DiagnosticTag::Deprecated:
			return i;
		}

		throw ParseError{};
	}
};


template <>
struct ParseValue<Trace> {
	static Trace go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kStringType) {
			throw ParseError{};
		}

		const auto s = std::string_view(v.GetString(), v.GetStringLength());

		if (s == "off")      return Trace::Off;
		if (s == "messages") return Trace::Messages;
		if (s == "verbose")  return Trace::Verbose;

		throw ParseError{};
	}
};


template <>
struct ParseValue<ResourceOperationKind> {
	static ResourceOperationKind go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kStringType) {
			throw ParseError{};
		}

		const auto s = std::string_view(v.GetString(), v.GetStringLength());

		if (s == "create") return ResourceOperationKind::Create;
		if (s == "rename") return ResourceOperationKind::Rename;
		if (s == "delete") return ResourceOperationKind::Delete;

		throw ParseError{};
	}
};


template <>
struct ParseValue<FailureHandlingKind> {
	static FailureHandlingKind go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kStringType) {
			throw ParseError{};
		}

		const auto s = std::string_view(v.GetString(), v.GetStringLength());

		if (s == "abort")                 return FailureHandlingKind::Abort;
		if (s == "transactional")         return FailureHandlingKind::Transactional;
		if (s == "undo")                  return FailureHandlingKind::Undo;
		if (s == "textOnlyTransactional") return FailureHandlingKind::TextOnlyTransactional;

		throw ParseError{};
	}
};


template <>
struct ParseValue<MarkupKind> {
	static MarkupKind go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kStringType) {
			throw ParseError{};
		}

		const auto s = std::string_view(v.GetString(), v.GetStringLength());

		if (s == "plaintext") return MarkupKind::PlainText;
		if (s == "markdown")  return MarkupKind::Markdown;

		throw ParseError{};
	}
};


template <>
struct ParseValue<CodeActionKind> {
	static CodeActionKind go(const rapidjson::Value & v)
	{
		if (v.GetType() != rapidjson::kStringType) {
			throw ParseError{};
		}

		const auto s = std::string_view(v.GetString(), v.GetStringLength());

		if (s == "")                       return CodeActionKind::Empty;
		if (s == "*")                      return CodeActionKind::All;
		if (s == "quickfix")               return CodeActionKind::QuickFix;
		if (s == "refactor")               return CodeActionKind::Refactor;
		if (s == "refactor.extract")       return CodeActionKind::RefactorExtract;
		if (s == "refactor.inline")        return CodeActionKind::RefactorInline;
		if (s == "refactor.rewrite")       return CodeActionKind::RefactorRewrite;
		if (s == "source")                 return CodeActionKind::Source;
		if (s == "source.organizeImports") return CodeActionKind::SourceOrganizeImports;

		throw ParseError{};
	}
};




template <typename Variant, typename... T> struct Parser
{
	static void parse(Variant &, const rapidjson::Value &)
	{
		throw ParseError{};
	}
};

template <typename Variant, typename Arg, typename... T>
struct Parser<Variant, Arg, T...> {
	static void parse(Variant & variant, const rapidjson::Value & value)
	{
		try {
			Arg arg = ParseValue<Arg>::go(value);
			variant = arg;
		} catch (const ParseError &) {
			Parser<Variant, T...>::parse(variant, value);
		}
	}
};




template <typename... T>
struct ParseValue<std::variant<T...>> {
	typedef std::variant<T...> Variant;

	static Variant go(const rapidjson::Value & v)
	{
		Variant variant;
		Parser<Variant, T...>::parse(variant, v);
		return variant;
	}
};




template <typename T>
void parseValue(const rapidjson::Value & v, T & value, const std::string_view & name);


template <typename T>
void parseValue(const rapidjson::Value & v, std::optional<T> & o, const std::string_view & name)
{
	const auto it = v.FindMember(rapidjson::Value(name.data(), name.size()));
	if (it == v.MemberEnd()) return;

	try {
		o = ParseValue<T>::go(it->value);
	} catch (const ParseError &) {
		FLAT_WARNING << "Parse error:" << name;
	}
}


template <typename T>
void parseValue(const rapidjson::Value & v, T & value, const std::string_view & name)
{
	const auto it = v.FindMember(rapidjson::Value(name.data(), name.size()));
	if (it == v.MemberEnd()) throw ParseError{};

	value = ParseValue<T>::go(it->value);
}




#define FLAT_LSP_PARSE_FIELD(NAME) \
	parseValue(v, p.NAME, #NAME);


template <>
CancelParams fromJson<CancelParams>(const rapidjson::Value & v)
{
	CancelParams p;
	FLAT_LSP_PARSE_FIELD(id)
	return p;
}


template <>
WorkDoneProgressParams fromJson<WorkDoneProgressParams>(const rapidjson::Value & v)
{
	WorkDoneProgressParams p;
	FLAT_LSP_PARSE_FIELD(workDoneToken)
	return p;
}


template <>
InitializeParams::ClientInfo fromJson<InitializeParams::ClientInfo>(const rapidjson::Value & v)
{
	InitializeParams::ClientInfo p;
	FLAT_LSP_PARSE_FIELD(name)
	FLAT_LSP_PARSE_FIELD(version)
	return p;
}


template <>
WorkspaceEditClientCapabilities fromJson<WorkspaceEditClientCapabilities>(
		const rapidjson::Value & v)
{
	WorkspaceEditClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(documentChanges)
	FLAT_LSP_PARSE_FIELD(resourceOperations)
	FLAT_LSP_PARSE_FIELD(failureHandling)
	return p;
}


template <>
DidChangeConfigurationClientCapabilities fromJson<DidChangeConfigurationClientCapabilities>(
		const rapidjson::Value & v)
{
	DidChangeConfigurationClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
DidChangeWatchedFilesClientCapabilities fromJson<DidChangeWatchedFilesClientCapabilities>(
		const rapidjson::Value & v)
{
	DidChangeWatchedFilesClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
WorkspaceSymbolClientCapabilities::SymbolKind
		fromJson<WorkspaceSymbolClientCapabilities::SymbolKind>(const rapidjson::Value & v)
{
	WorkspaceSymbolClientCapabilities::SymbolKind p;
	FLAT_LSP_PARSE_FIELD(valueSet)
	return p;
}


template <>
WorkspaceSymbolClientCapabilities fromJson<WorkspaceSymbolClientCapabilities>(
		const rapidjson::Value & v)
{
	WorkspaceSymbolClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(symbolKind)
	return p;
}


template <>
ExecuteCommandClientCapabilities fromJson<ExecuteCommandClientCapabilities>(
		const rapidjson::Value & v)
{
	ExecuteCommandClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
ClientCapabilities::Workspace fromJson<ClientCapabilities::Workspace>(const rapidjson::Value & v)
{
	ClientCapabilities::Workspace p;
	FLAT_LSP_PARSE_FIELD(applyEdit)
	FLAT_LSP_PARSE_FIELD(workspaceEdit)
	FLAT_LSP_PARSE_FIELD(didChangeConfiguration)
	FLAT_LSP_PARSE_FIELD(didChangeWatchedFiles)
	FLAT_LSP_PARSE_FIELD(symbol)
	FLAT_LSP_PARSE_FIELD(executeCommand)
	FLAT_LSP_PARSE_FIELD(workspaceFolders)
	FLAT_LSP_PARSE_FIELD(configuration)
	return p;
}


template <>
ClientCapabilities::Window fromJson<ClientCapabilities::Window>(const rapidjson::Value & v)
{
	ClientCapabilities::Window p;
	FLAT_LSP_PARSE_FIELD(workDoneProgress)
	return p;
}


template <>
TextDocumentSyncClientCapabilities fromJson<TextDocumentSyncClientCapabilities>(
		const rapidjson::Value & v)
{
	TextDocumentSyncClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(willSave)
	FLAT_LSP_PARSE_FIELD(willSaveWaitUntil)
	FLAT_LSP_PARSE_FIELD(didSave)
	return p;
}


template <>
CompletionClientCapabilities::CompletionItem::TagSupport
		fromJson<CompletionClientCapabilities::CompletionItem::TagSupport>(
			const rapidjson::Value & v)
{
	CompletionClientCapabilities::CompletionItem::TagSupport p;
	FLAT_LSP_PARSE_FIELD(valueSet)
	return p;
}


template <>
CompletionClientCapabilities::CompletionItem fromJson<CompletionClientCapabilities::CompletionItem>(
		const rapidjson::Value & v)
{
	CompletionClientCapabilities::CompletionItem p;
	FLAT_LSP_PARSE_FIELD(snippetSupport)
	FLAT_LSP_PARSE_FIELD(commitCharactersSupport)
	FLAT_LSP_PARSE_FIELD(documentationFormat)
	FLAT_LSP_PARSE_FIELD(deprecatedSupport)
	FLAT_LSP_PARSE_FIELD(preselectSupport)
	FLAT_LSP_PARSE_FIELD(tagSupport)
	return p;
}


template <>
CompletionClientCapabilities::CompletionItemKind
		fromJson<CompletionClientCapabilities::CompletionItemKind>(const rapidjson::Value & v)
{
	CompletionClientCapabilities::CompletionItemKind p;
	FLAT_LSP_PARSE_FIELD(valueSet)
	return p;
}


template <>
CompletionClientCapabilities fromJson<CompletionClientCapabilities>(const rapidjson::Value & v)
{
	CompletionClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(completionItem)
	FLAT_LSP_PARSE_FIELD(completionItemKind)
	FLAT_LSP_PARSE_FIELD(contextSupport)
	return p;
}


template <>
HoverClientCapabilities fromJson<HoverClientCapabilities>(const rapidjson::Value & v)
{
	HoverClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(contentFormat)
	return p;
}


template <>
SignatureHelpClientCapabilities::SignatureInformation::ParameterInformation
		fromJson<SignatureHelpClientCapabilities::SignatureInformation::ParameterInformation>(
			const rapidjson::Value & v)
{
	SignatureHelpClientCapabilities::SignatureInformation::ParameterInformation p;
	FLAT_LSP_PARSE_FIELD(labelOffsetSupport)
	return p;
}


template <>
SignatureHelpClientCapabilities::SignatureInformation
		fromJson<SignatureHelpClientCapabilities::SignatureInformation>(const rapidjson::Value & v)
{
	SignatureHelpClientCapabilities::SignatureInformation p;
	FLAT_LSP_PARSE_FIELD(documentationFormat)
	FLAT_LSP_PARSE_FIELD(parameterInformation)
	return p;
}


template <>
SignatureHelpClientCapabilities fromJson<SignatureHelpClientCapabilities>(
		const rapidjson::Value & v)
{
	SignatureHelpClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(signatureInformation)
	FLAT_LSP_PARSE_FIELD(contextSupport)
	return p;
}


template <>
DeclarationClientCapabilities fromJson<DeclarationClientCapabilities>(const rapidjson::Value & v)
{
	DeclarationClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(linkSupport)
	return p;
}


template <>
DefinitionClientCapabilities fromJson<DefinitionClientCapabilities>(const rapidjson::Value & v)
{
	DefinitionClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(linkSupport)
	return p;
}


template <>
TypeDefinitionClientCapabilities fromJson<TypeDefinitionClientCapabilities>(
		const rapidjson::Value & v)
{
	TypeDefinitionClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(linkSupport)
	return p;
}


template <>
ImplementationClientCapabilities fromJson<ImplementationClientCapabilities>(
		const rapidjson::Value & v)
{
	ImplementationClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(linkSupport)
	return p;
}


template <>
ReferenceClientCapabilities fromJson<ReferenceClientCapabilities>(const rapidjson::Value & v)
{
	ReferenceClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
DocumentHighlightClientCapabilities fromJson<DocumentHighlightClientCapabilities>(
		const rapidjson::Value & v)
{
	DocumentHighlightClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
DocumentSymbolClientCapabilities::SymbolKind fromJson<DocumentSymbolClientCapabilities::SymbolKind>(
		const rapidjson::Value & v)
{
	DocumentSymbolClientCapabilities::SymbolKind p;
	FLAT_LSP_PARSE_FIELD(valueSet)
	return p;
}


template <>
DocumentSymbolClientCapabilities fromJson<DocumentSymbolClientCapabilities>(
		const rapidjson::Value & v)
{
	DocumentSymbolClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(symbolKind)
	FLAT_LSP_PARSE_FIELD(hierarchicalDocumentSymbolSupport)
	return p;
}


template <>
CodeActionClientCapabilities::CodeActionLiteralSupport::CodeActionKind
		fromJson<CodeActionClientCapabilities::CodeActionLiteralSupport::CodeActionKind>(
			const rapidjson::Value & v)
{
	CodeActionClientCapabilities::CodeActionLiteralSupport::CodeActionKind p;
	FLAT_LSP_PARSE_FIELD(valueSet)
	return p;
}


template <>
CodeActionClientCapabilities::CodeActionLiteralSupport
		fromJson<CodeActionClientCapabilities::CodeActionLiteralSupport>(const rapidjson::Value & v)
{
	CodeActionClientCapabilities::CodeActionLiteralSupport p;
	FLAT_LSP_PARSE_FIELD(codeActionKind)
	return p;
}


template <>
CodeActionClientCapabilities fromJson<CodeActionClientCapabilities>(const rapidjson::Value & v)
{
	CodeActionClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(codeActionLiteralSupport)
	FLAT_LSP_PARSE_FIELD(isPreferredSupport)
	return p;
}


template <>
CodeLensClientCapabilities fromJson<CodeLensClientCapabilities>(const rapidjson::Value & v)
{
	CodeLensClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
DocumentLinkClientCapabilities fromJson<DocumentLinkClientCapabilities>(const rapidjson::Value & v)
{
	DocumentLinkClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(tooltipSupport)
	return p;
}


template <>
DocumentColorClientCapabilities fromJson<DocumentColorClientCapabilities>(
		const rapidjson::Value & v)
{
	DocumentColorClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
DocumentFormattingClientCapabilities fromJson<DocumentFormattingClientCapabilities>(
		const rapidjson::Value & v)
{
	DocumentFormattingClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
DocumentRangeFormattingClientCapabilities fromJson<DocumentRangeFormattingClientCapabilities>(
		const rapidjson::Value & v)
{
	DocumentRangeFormattingClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
DocumentOnTypeFormattingClientCapabilities fromJson<DocumentOnTypeFormattingClientCapabilities>(
		const rapidjson::Value & v)
{
	DocumentOnTypeFormattingClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
RenameClientCapabilities fromJson<RenameClientCapabilities>(const rapidjson::Value & v)
{
	RenameClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(prepareSupport)
	return p;
}


template <>
PublishDiagnosticsClientCapabilities::TagSupport
		fromJson<PublishDiagnosticsClientCapabilities::TagSupport>(const rapidjson::Value & v)
{
	PublishDiagnosticsClientCapabilities::TagSupport p;
	FLAT_LSP_PARSE_FIELD(valueSet)
	return p;
}


template <>
PublishDiagnosticsClientCapabilities fromJson<PublishDiagnosticsClientCapabilities>(
		const rapidjson::Value & v)
{
	PublishDiagnosticsClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(relatedInformation)
	FLAT_LSP_PARSE_FIELD(tagSupport)
	FLAT_LSP_PARSE_FIELD(versionSupport)
	return p;
}


template <>
FoldingRangeClientCapabilities fromJson<FoldingRangeClientCapabilities>(const rapidjson::Value & v)
{
	FoldingRangeClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	FLAT_LSP_PARSE_FIELD(rangeLimit)
	FLAT_LSP_PARSE_FIELD(lineFoldingOnly)
	return p;
}


template <>
SelectionRangeClientCapabilities fromJson<SelectionRangeClientCapabilities>(
		const rapidjson::Value & v)
{
	SelectionRangeClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(dynamicRegistration)
	return p;
}


template <>
TextDocumentClientCapabilities fromJson<TextDocumentClientCapabilities>(const rapidjson::Value & v)
{
	TextDocumentClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(synchronization)
	FLAT_LSP_PARSE_FIELD(completion)
	FLAT_LSP_PARSE_FIELD(hover)
	FLAT_LSP_PARSE_FIELD(signatureHelp)
	FLAT_LSP_PARSE_FIELD(declaration)
	FLAT_LSP_PARSE_FIELD(definition)
	FLAT_LSP_PARSE_FIELD(typeDefinition)
	FLAT_LSP_PARSE_FIELD(implementation)
	FLAT_LSP_PARSE_FIELD(references)
	FLAT_LSP_PARSE_FIELD(documentHighlight)
	FLAT_LSP_PARSE_FIELD(documentSymbol)
	FLAT_LSP_PARSE_FIELD(codeAction)
	FLAT_LSP_PARSE_FIELD(codeLens)
	FLAT_LSP_PARSE_FIELD(documentLink)
	FLAT_LSP_PARSE_FIELD(colorProvider)
	FLAT_LSP_PARSE_FIELD(formatting)
	FLAT_LSP_PARSE_FIELD(rangeFormatting)
	FLAT_LSP_PARSE_FIELD(onTypeFormatting)
	FLAT_LSP_PARSE_FIELD(rename)
	FLAT_LSP_PARSE_FIELD(publishDiagnostics)
	FLAT_LSP_PARSE_FIELD(foldingRange)
	FLAT_LSP_PARSE_FIELD(selectionRange)
	return p;
}


template <>
ClientCapabilities fromJson<ClientCapabilities>(const rapidjson::Value & v)
{
	ClientCapabilities p;
	FLAT_LSP_PARSE_FIELD(workspace)
	FLAT_LSP_PARSE_FIELD(textDocument)
	FLAT_LSP_PARSE_FIELD(window)
	FLAT_LSP_PARSE_FIELD(experimental)
	return p;
}


template <>
WorkspaceFolder fromJson<WorkspaceFolder>(const rapidjson::Value & v)
{
	WorkspaceFolder p;
	FLAT_LSP_PARSE_FIELD(uri)
	FLAT_LSP_PARSE_FIELD(name)
	return p;
}


template <>
InitializeParams fromJson<InitializeParams>(const rapidjson::Value & v)
{
	InitializeParams p;
	static_cast<WorkDoneProgressParams&>(p) = fromJson<WorkDoneProgressParams>(v);
	FLAT_LSP_PARSE_FIELD(processId)
	FLAT_LSP_PARSE_FIELD(clientInfo)
	FLAT_LSP_PARSE_FIELD(rootPath)
	FLAT_LSP_PARSE_FIELD(rootUri)
	FLAT_LSP_PARSE_FIELD(initializationOptions)
	FLAT_LSP_PARSE_FIELD(capabilities)
	FLAT_LSP_PARSE_FIELD(trace)
	FLAT_LSP_PARSE_FIELD(workspaceFolders)
	return p;
}


template <>
InitializedParams fromJson<InitializedParams>(const rapidjson::Value &)
{
	return {};
}


template <>
TextDocumentItem fromJson<TextDocumentItem>(const rapidjson::Value & v)
{
	TextDocumentItem p;
	FLAT_LSP_PARSE_FIELD(uri)
	FLAT_LSP_PARSE_FIELD(languageId)
	FLAT_LSP_PARSE_FIELD(version)
	FLAT_LSP_PARSE_FIELD(text)
	return p;
}


template <>
DidOpenTextDocumentParams fromJson<DidOpenTextDocumentParams>(const rapidjson::Value & v)
{
	DidOpenTextDocumentParams p;
	FLAT_LSP_PARSE_FIELD(textDocument)
	return p;
}


template <>
TextDocumentIdentifier fromJson<TextDocumentIdentifier>(const rapidjson::Value & v)
{
	TextDocumentIdentifier p;
	FLAT_LSP_PARSE_FIELD(uri)
	return p;
}


template <>
DidCloseTextDocumentParams fromJson<DidCloseTextDocumentParams>(const rapidjson::Value & v)
{
	DidCloseTextDocumentParams p;
	FLAT_LSP_PARSE_FIELD(textDocument)
	return p;
}


template <>
VersionedTextDocumentIdentifier fromJson<VersionedTextDocumentIdentifier>(
		const rapidjson::Value & v)
{
	VersionedTextDocumentIdentifier p;
	static_cast<TextDocumentIdentifier&>(p) = fromJson<TextDocumentIdentifier>(v);
	FLAT_LSP_PARSE_FIELD(version)
	return p;
}


template <>
Position fromJson<Position>(const rapidjson::Value & v)
{
	Position p;
	FLAT_LSP_PARSE_FIELD(line)
	FLAT_LSP_PARSE_FIELD(character)
	return p;
}


template <>
Range fromJson<Range>(const rapidjson::Value & v)
{
	Range p;
	FLAT_LSP_PARSE_FIELD(start)
	FLAT_LSP_PARSE_FIELD(end)
	return p;
}


template <>
TextDocumentContentChangeEvent fromJson<TextDocumentContentChangeEvent>(const rapidjson::Value & v)
{
	TextDocumentContentChangeEvent p;
	FLAT_LSP_PARSE_FIELD(range)
	FLAT_LSP_PARSE_FIELD(rangeLength)
	FLAT_LSP_PARSE_FIELD(text)
	return p;
}


template <>
DidChangeTextDocumentParams fromJson<DidChangeTextDocumentParams>(const rapidjson::Value & v)
{
	DidChangeTextDocumentParams p;
	FLAT_LSP_PARSE_FIELD(textDocument)
	FLAT_LSP_PARSE_FIELD(contentChanges)
	return p;
}


template <>
DidSaveTextDocumentParams fromJson<DidSaveTextDocumentParams>(const rapidjson::Value & v)
{
	DidSaveTextDocumentParams p;
	FLAT_LSP_PARSE_FIELD(textDocument)
	FLAT_LSP_PARSE_FIELD(text)
	return p;
}


template <>
PartialResultParams fromJson<PartialResultParams>(const rapidjson::Value & v)
{
	PartialResultParams p;
	FLAT_LSP_PARSE_FIELD(partialResultToken)
	return p;
}


template <>
DocumentSymbolParams fromJson<DocumentSymbolParams>(const rapidjson::Value & v)
{
	DocumentSymbolParams p;
	static_cast<WorkDoneProgressParams&>(p) = fromJson<WorkDoneProgressParams>(v);
	static_cast<PartialResultParams&>(p) = fromJson<PartialResultParams>(v);
	FLAT_LSP_PARSE_FIELD(textDocument)
	return p;
}


template <>
TextDocumentPositionParams fromJson<TextDocumentPositionParams>(const rapidjson::Value & v)
{
	TextDocumentPositionParams p;
	FLAT_LSP_PARSE_FIELD(textDocument)
	FLAT_LSP_PARSE_FIELD(position)
	return p;
}


template <>
DocumentHighlightParams fromJson<DocumentHighlightParams>(const rapidjson::Value & v)
{
	DocumentHighlightParams p;
	static_cast<TextDocumentPositionParams&>(p) = fromJson<TextDocumentPositionParams>(v);
	static_cast<WorkDoneProgressParams&>(p) = fromJson<WorkDoneProgressParams>(v);
	static_cast<PartialResultParams&>(p) = fromJson<PartialResultParams>(v);
	return p;
}


template <>
ReferenceContext fromJson<ReferenceContext>(const rapidjson::Value & v)
{
	ReferenceContext p;
	FLAT_LSP_PARSE_FIELD(includeDeclaration)
	return p;
}


template <>
ReferenceParams fromJson<ReferenceParams>(const rapidjson::Value & v)
{
	ReferenceParams p;
	static_cast<TextDocumentPositionParams&>(p) = fromJson<TextDocumentPositionParams>(v);
	static_cast<WorkDoneProgressParams&>(p) = fromJson<WorkDoneProgressParams>(v);
	static_cast<PartialResultParams&>(p) = fromJson<PartialResultParams>(v);
	FLAT_LSP_PARSE_FIELD(context)
	return p;
}


template <>
DefinitionParams fromJson<DefinitionParams>(const rapidjson::Value & v)
{
	DefinitionParams p;
	static_cast<TextDocumentPositionParams&>(p) = fromJson<TextDocumentPositionParams>(v);
	static_cast<WorkDoneProgressParams&>(p) = fromJson<WorkDoneProgressParams>(v);
	static_cast<PartialResultParams&>(p) = fromJson<PartialResultParams>(v);
	return p;
}




#define FLAT_LSP_WRITE_FIELD(NAME) \
	JsonValueWriter<std::decay_t<decltype(p.NAME)>>::go(*this, p.NAME, #NAME);


struct JsonObjectWriter {
	rapidjson::Document::AllocatorType & allocator;
	rapidjson::Value & object;

	template <typename T> void go(const T & p);
};


template <typename T>
struct JsonWriter {
	static rapidjson::Value go(const T & p, rapidjson::Document::AllocatorType & allocator);
};


template <typename T>
rapidjson::Value JsonWriter<T>::go(const T & p,
		rapidjson::Document::AllocatorType & allocator)
{
	rapidjson::Value object{rapidjson::kObjectType};
	JsonObjectWriter w{allocator, object};
	w.go(p);
	return object;
}


template <typename... T>
struct JsonWriter<std::variant<T...>> {
	static rapidjson::Value go(const std::variant<T...> & p,
			rapidjson::Document::AllocatorType & allocator)
	{
		rapidjson::Value v;
		std::visit([&] (auto && val) {
			typedef std::decay_t<decltype(val)> ValT;
			v = JsonWriter<ValT>::go(val, allocator);
		}, p);
		return v;
	}
};


template <typename T>
struct JsonWriter<std::vector<T>> {
	static rapidjson::Value go(const std::vector<T> & p,
			rapidjson::Document::AllocatorType & allocator)
	{
		rapidjson::Value v(rapidjson::kArrayType);
		for (const auto & val : p) {
			v.PushBack(JsonWriter<T>::go(val, allocator), allocator);
		}
		return v;
	}
};


template <>
struct JsonWriter<std::nullptr_t> {
	static rapidjson::Value go(const std::nullptr_t &, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(rapidjson::kNullType);
	}
};


template <>
struct JsonWriter<bool> {
	static rapidjson::Value go(const bool & p, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(p ? rapidjson::kTrueType : rapidjson::kFalseType);
	}
};


template <>
struct JsonWriter<int> {
	static rapidjson::Value go(const int & p, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(p);
	}
};


template <>
struct JsonWriter<std::string> {
	static rapidjson::Value go(const std::string & p, rapidjson::Document::AllocatorType & allocator)
	{
		return rapidjson::Value(p.data(), p.size(), allocator);
	}
};


template <>
struct JsonWriter<TextDocumentSyncKind> {
	static rapidjson::Value go(const TextDocumentSyncKind & p, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(int(p));
	}
};


template <>
struct JsonWriter<SymbolKind> {
	static rapidjson::Value go(const SymbolKind & p, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(int(p));
	}
};


template <>
struct JsonWriter<DocumentHighlightKind> {
	static rapidjson::Value go(const DocumentHighlightKind & p, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(int(p));
	}
};


template <>
struct JsonWriter<CompletionItemKind> {
	static rapidjson::Value go(const CompletionItemKind & p, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(int(p));
	}
};


template <>
struct JsonWriter<DiagnosticSeverity> {
	static rapidjson::Value go(const DiagnosticSeverity & p, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(int(p));
	}
};


template <>
struct JsonWriter<DiagnosticTag> {
	static rapidjson::Value go(const DiagnosticTag & p, rapidjson::Document::AllocatorType &)
	{
		return rapidjson::Value(int(p));
	}
};




template <typename T>
struct JsonValueWriter {
	static void go(JsonObjectWriter & w, const T & o, const std::string_view & name)
	{
		w.object.AddMember(rapidjson::Value(name.data(), name.size()),
				JsonWriter<T>::go(o, w.allocator), w.allocator);
	}
};


template <typename T>
struct JsonValueWriter<std::optional<T>> {
	static void go(JsonObjectWriter & w, const std::optional<T> & o, const std::string_view & name)
	{
		if (!o.has_value()) return;
		JsonValueWriter<T>::go(w, o.value(), name);
	}
};


template <>
void JsonObjectWriter::go<TextDocumentSyncOptions>(const TextDocumentSyncOptions & p)
{
	FLAT_LSP_WRITE_FIELD(openClose)
	FLAT_LSP_WRITE_FIELD(change)
}


template <>
void JsonObjectWriter::go<WorkDoneProgressOptions>(const WorkDoneProgressOptions & p)
{
	FLAT_LSP_WRITE_FIELD(workDoneProgress)
}


template <>
void JsonObjectWriter::go<CompletionOptions>(const CompletionOptions & p)
{
	go(static_cast<const WorkDoneProgressOptions&>(p));
	FLAT_LSP_WRITE_FIELD(triggerCharacters)
	FLAT_LSP_WRITE_FIELD(allCommitCharacters)
	FLAT_LSP_WRITE_FIELD(resolveProvider)
}


template <>
void JsonObjectWriter::go<HoverOptions>(const HoverOptions & p)
{
	go(static_cast<const WorkDoneProgressOptions&>(p));
}


template <>
void JsonObjectWriter::go<SignatureHelpOptions>(const SignatureHelpOptions & p)
{
	go(static_cast<const WorkDoneProgressOptions&>(p));
	FLAT_LSP_WRITE_FIELD(triggerCharacters)
	FLAT_LSP_WRITE_FIELD(retriggerCharacters)
}


template <>
void JsonObjectWriter::go<ReferenceOptions>(const ReferenceOptions & p)
{
	go(static_cast<const WorkDoneProgressOptions&>(p));
}


template <>
void JsonObjectWriter::go<DocumentHighlightOptions>(const DocumentHighlightOptions & p)
{
	go(static_cast<const WorkDoneProgressOptions&>(p));
}


template <>
void JsonObjectWriter::go<DocumentSymbolOptions>(const DocumentSymbolOptions & p)
{
	go(static_cast<const WorkDoneProgressOptions&>(p));
}


template <>
void JsonObjectWriter::go<ServerCapabilities>(const ServerCapabilities & p)
{
	FLAT_LSP_WRITE_FIELD(textDocumentSync)
	FLAT_LSP_WRITE_FIELD(completionProvider)
	FLAT_LSP_WRITE_FIELD(hoverProvider)
	FLAT_LSP_WRITE_FIELD(signatureHelpProvider)
//	FLAT_LSP_WRITE_FIELD(declarationProvider)
//	FLAT_LSP_WRITE_FIELD(definitionProvider)
//	FLAT_LSP_WRITE_FIELD(typeDefinitionProvider)
//	FLAT_LSP_WRITE_FIELD(implementationProvider)
	FLAT_LSP_WRITE_FIELD(referencesProvider)
	FLAT_LSP_WRITE_FIELD(documentHighlightProvider)
	FLAT_LSP_WRITE_FIELD(documentSymbolProvider)
//	FLAT_LSP_WRITE_FIELD(codeActionProvider)
//	FLAT_LSP_WRITE_FIELD(codeLensProvider)
//	FLAT_LSP_WRITE_FIELD(documentLinkProvider)
//	FLAT_LSP_WRITE_FIELD(colorProvider)
//	FLAT_LSP_WRITE_FIELD(documentFormattingProvider)
//	FLAT_LSP_WRITE_FIELD(documentRangeFormattingProvider)
//	FLAT_LSP_WRITE_FIELD(documentOnTypeFormattingProvider)
//	FLAT_LSP_WRITE_FIELD(renameProvider)
//	FLAT_LSP_WRITE_FIELD(foldingRangeProvider)
//	FLAT_LSP_WRITE_FIELD(executeCommandProvider)
//	FLAT_LSP_WRITE_FIELD(selectionRangeProvider)
//	FLAT_LSP_WRITE_FIELD(workspaceSymbolProvider)
//	FLAT_LSP_WRITE_FIELD(workspace)
//	FLAT_LSP_WRITE_FIELD(experimental)
}


template <>
void JsonObjectWriter::go<InitializeResult>(const InitializeResult & p)
{
	FLAT_LSP_WRITE_FIELD(capabilities)
}


template <>
void JsonObjectWriter::go<Position>(const Position & p)
{
	FLAT_LSP_WRITE_FIELD(line)
	FLAT_LSP_WRITE_FIELD(character)
}


template <>
void JsonObjectWriter::go<Range>(const Range & p)
{
	FLAT_LSP_WRITE_FIELD(start)
	FLAT_LSP_WRITE_FIELD(end)
}


template <>
void JsonObjectWriter::go<Location>(const Location & p)
{
	FLAT_LSP_WRITE_FIELD(uri)
	FLAT_LSP_WRITE_FIELD(range)
}


template <>
void JsonObjectWriter::go<LocationLink>(const LocationLink & p)
{
	FLAT_LSP_WRITE_FIELD(originSelectionRange)
	FLAT_LSP_WRITE_FIELD(targetUri)
	FLAT_LSP_WRITE_FIELD(targetRange)
	FLAT_LSP_WRITE_FIELD(targetSelectionRange)
}


template <>
void JsonObjectWriter::go<DocumentSymbol>(const DocumentSymbol & p)
{
	FLAT_LSP_WRITE_FIELD(name)
	FLAT_LSP_WRITE_FIELD(detail)
	FLAT_LSP_WRITE_FIELD(kind)
	FLAT_LSP_WRITE_FIELD(deprecated)
	FLAT_LSP_WRITE_FIELD(range)
	FLAT_LSP_WRITE_FIELD(selectionRange)
	FLAT_LSP_WRITE_FIELD(children)
}


template <>
void JsonObjectWriter::go<SymbolInformation>(const SymbolInformation & p)
{
	FLAT_LSP_WRITE_FIELD(name)
	FLAT_LSP_WRITE_FIELD(kind)
	FLAT_LSP_WRITE_FIELD(deprecated)
	FLAT_LSP_WRITE_FIELD(location)
	FLAT_LSP_WRITE_FIELD(containerName)
}


template <>
void JsonObjectWriter::go<DocumentHighlight>(const DocumentHighlight & p)
{
	FLAT_LSP_WRITE_FIELD(range)
	FLAT_LSP_WRITE_FIELD(kind)
}


template <>
void JsonObjectWriter::go<DiagnosticRelatedInformation>(const DiagnosticRelatedInformation & p)
{
	FLAT_LSP_WRITE_FIELD(location)
	FLAT_LSP_WRITE_FIELD(message)
}


template <>
void JsonObjectWriter::go<Diagnostic>(const Diagnostic & p)
{
	FLAT_LSP_WRITE_FIELD(range)
	FLAT_LSP_WRITE_FIELD(severity)
	FLAT_LSP_WRITE_FIELD(code)
	FLAT_LSP_WRITE_FIELD(source)
	FLAT_LSP_WRITE_FIELD(message)
	FLAT_LSP_WRITE_FIELD(tags)
	FLAT_LSP_WRITE_FIELD(relatedInformation)
}


template <>
void JsonObjectWriter::go<PublishDiagnosticsParams>(const PublishDiagnosticsParams & p)
{
	FLAT_LSP_WRITE_FIELD(uri)
	FLAT_LSP_WRITE_FIELD(version)
	FLAT_LSP_WRITE_FIELD(diagnostics)
}




#define FLAT_LSP_JSONWRITER_TYPE(TYPE) \
	template <> rapidjson::Value toJson<TYPE>(const TYPE & p, \
			rapidjson::Document::AllocatorType & allocator) \
	{ \
		return JsonWriter<TYPE>::go(p, allocator); \
	}

FLAT_LSP_JSONWRITER_TYPE(InitializeResult)
FLAT_LSP_JSONWRITER_TYPE(DocumentSymbolResult)
FLAT_LSP_JSONWRITER_TYPE(DocumentHighlightResult)
FLAT_LSP_JSONWRITER_TYPE(DefinitionResult)
FLAT_LSP_JSONWRITER_TYPE(ReferencesResult)
FLAT_LSP_JSONWRITER_TYPE(PublishDiagnosticsParams)




}}
