
#include "JsonRpc.hpp"

#include <rapidjson/writer.h>

#include "Debug.hpp"




namespace Flat {
namespace Lsp {




static constexpr std::string_view kContentLengthField = "Content-Length";
static constexpr std::string_view kContentTypeField   = "Content-Type";

static constexpr std::string_view kJsonRpcMimeType = "application/vscode-jsonrpc; charset=utf-8";

// protect code from reading invalid stream forever
static constexpr int kMaxHeaderSize = 1024 * 1024;
static constexpr int kMaxDataSize = 1024 * 1024;




std::string_view JsonRpc::jsonStringToStringView(const rapidjson::Value & v) noexcept
{
	return std::string_view(v.GetString(), v.GetStringLength());
}


std::string JsonRpc::jsonValueToString(const rapidjson::Value & v) noexcept
{
	rapidjson::StringBuffer buffer;

	rapidjson::Writer w(buffer);
	const bool ok = v.Accept(w);
	FLAT_UNUSED_ASSERT(ok);

	return std::string(buffer.GetString(), buffer.GetSize());
}




JsonRpc::Call::Call(JsonRpc & jsonRpc, const CallType type,
		std::optional<rapidjson::Document> requestDocument, rapidjson::Value method,
		std::optional<rapidjson::Value> params) :
	jsonRpc_(jsonRpc),
	type_(type),
	requestDocument_(std::move(requestDocument)),
	method_(std::move(method)),
	params_(std::move(params))
{
	methodString_ = jsonStringToStringView(method_);
}


std::string_view JsonRpc::Call::method() const noexcept
{
	return jsonStringToStringView(method_);
}




JsonRpc::Notification::Notification(JsonRpc & jsonRpc,
		std::optional<rapidjson::Document> requestDocument, rapidjson::Value method,
		std::optional<rapidjson::Value> params) :
	Call(jsonRpc, CallType::Notification, std::move(requestDocument), std::move(method),
			std::move(params))
{
}




JsonRpc::Request::Request(JsonRpc & jsonRpc, std::optional<rapidjson::Document> requestDocument,
		rapidjson::Value id, rapidjson::Value method, std::optional<rapidjson::Value> params,
		BatchInfo * const batchInfo) :
	Call(jsonRpc, CallType::Request, std::move(requestDocument), std::move(method), std::move(params)),
	id_(std::move(id)),
	response_(batchInfo ? Response(BatchResponse{batchInfo}) : Response(StandaloneResponse{}))
{
	idString_ = jsonStringToStringView(id_);
}


bool JsonRpc::Request::isDone() const noexcept
{
	return std::visit([this] (auto && response) -> bool {
		using T = std::decay_t<decltype(response)>;

		if constexpr (std::is_same_v<T, Request::StandaloneResponse>) {
			return response.document.has_value();
		}

		if constexpr (std::is_same_v<T, Request::BatchResponse>) {
			const BatchInfo::R & r = JsonRpc::_getBatchRequest(*this);
			return r.value.has_value();
		}
	}, response_);
}




JsonRpc::JsonRpc(Callbacks & callbacks) :
	callbacks_(callbacks)
{
}


JsonRpc::~JsonRpc()
{
	FLAT_ASSERT(requests_.empty());
}


JsonRpc::CallError JsonRpc::_throwInvalidRequestErr()
{
	throw CallError{ErrorCode::InvalidRequest, "Invalid request", {}};
}


void JsonRpc::read(Reader & reader)
{
	while (true) {
		if (readingState_ == ReadingState::Length) {
			if (!_readLength(reader)) {
				// length is still incomplete
				return;
			}
		}

		if (!_readData(reader)) {
			// data is still incomplete
			return;
		}

		if (contentType_ == ContentType::Unknown) {
			// skip unknown content
			continue;
		}

		// parse json
		rapidjson::StringStream stream(reinterpret_cast<const char*>(dataBuffer_.data()));

		rapidjson::Document requestDocument;
		requestDocument.ParseStream(stream);

		const auto processError = [this] (CallError & e) {
			rapidjson::Document doc;
			doc.SetObject() = _createErrorResponse({}, e.code, e.message, std::move(e.data),
					doc.GetAllocator());
			_sendMessage(doc);
		};

		try {
			const int readStreamSize = stream.src_ - stream.head_;
			if (readStreamSize != contentLength_ ||
					requestDocument.GetParseError() != rapidjson::kParseErrorNone) {
				FLAT_WARNING << "Invalid JSON";
				throw CallError{ErrorCode::ParseError, "Parse error", {}};
			}
		} catch (CallError & e) {
			processError(e);
		}

		try {
			const auto invokeCall = [this] (std::unique_ptr<Call> call) {
				switch (call->type()) {
				case CallType::Notification:
					callbacks_.notification(static_cast<Notification&>(*call));
					break;
				case CallType::Request:
					callbacks_.request(std::unique_ptr<Request>(
							static_cast<Request*>(call.release())));
					break;
				}
			};

			if (requestDocument.IsObject()) {
				std::unique_ptr<Call> call = _createCall(&requestDocument, requestDocument, {});
				invokeCall(std::move(call));
			} else if (requestDocument.IsArray()) {
				auto callArray = requestDocument.GetArray();
				if (callArray.Empty()) _throwInvalidRequestErr();

				std::unique_ptr<BatchInfo> batchInfo;

				const GetBatchInfo getBatchInfo = [&batchInfo] () -> BatchInfo & {
					if (!batchInfo) {
						batchInfo.reset(new BatchInfo);
					}
					return *batchInfo;
				};

				std::vector<std::unique_ptr<Call>> calls;
				for (auto & c : callArray) {
					calls.push_back(_createCall(nullptr, c, getBatchInfo));
				}

				if (batchInfo) {
					batchInfos_.push_back(std::move(batchInfo));
				}

				for (auto & call : calls) {
					invokeCall(std::move(call));
				}
			} else {
				FLAT_WARNING << "Bad JSON";
				_throwInvalidRequestErr();
			}
		} catch (CallError & e) {
			processError(e);
		}
	}
}


void JsonRpc::write(Writer & writer)
{
	int totalBytesWritten = 0;

	outputBuffer_.visit([&totalBytesWritten, &writer] (
			const uint8_t * const data, const int size) ->bool {
		const int bytesWritten = writer.write(data, size);
		totalBytesWritten += bytesWritten;
		return bytesWritten == size;
	});

	outputBuffer_.pop(totalBytesWritten);
}


void JsonRpc::sendNotification(const NotificationCallback cb)
{
	rapidjson::Document doc;
	doc.SetObject() = _createMessage(doc.GetAllocator());

	NotificationCallbackInfo info = cb(doc.GetAllocator());
	doc.AddMember("method", rapidjson::Value(info.method.data(), info.method.size()),
			doc.GetAllocator());
	doc.AddMember("params", std::move(info.params), doc.GetAllocator());

	FLAT_DEBUG << jsonValueToString(doc);

	_sendMessage(doc);
}


template <typename Allocator>
rapidjson::Value JsonRpc::_createMessage(Allocator & allocator) const noexcept
{
	rapidjson::Value response(rapidjson::kObjectType);
	response.AddMember("jsonrpc", "2.0", allocator);
	return response;
}


template <typename Allocator>
rapidjson::Value JsonRpc::_createMessageWithId(const rapidjson::Value & id,
		Allocator & allocator) const noexcept
{
	rapidjson::Value response = _createMessage(allocator);
	response.AddMember("id", rapidjson::Value(id.GetString(), allocator), allocator);
	return response;
}


template <typename Allocator>
rapidjson::Value JsonRpc::_createErrorResponse(const rapidjson::Value & id, const ErrorCode code,
		const std::string_view message, std::optional<rapidjson::Value> data,
		Allocator & allocator) const noexcept
{
	auto response = _createMessageWithId(id, allocator);

	rapidjson::Value error(rapidjson::kObjectType);
	error.AddMember("code", int(code), allocator);
	error.AddMember("message", rapidjson::Value(message.data(), message.size(), allocator),
			allocator);
	if (data) {
		error.AddMember("data", std::move(*data), allocator);
	}

	response.AddMember("error", std::move(error), allocator);

	return response;
}


std::unique_ptr<JsonRpc::Call> JsonRpc::_createCall(
		rapidjson::Document * const requestDocumentPtr, rapidjson::Value & value,
		const GetBatchInfo getBatchInfo)
{
	const auto object = value.GetObject();

	{
		const auto it = object.FindMember("jsonrpc");
		if (it == object.MemberEnd()) _throwInvalidRequestErr();
		const auto & v = it->value;
		if (!v.IsString()) _throwInvalidRequestErr();
		const auto value = jsonStringToStringView(v);
		if (value != "2.0") _throwInvalidRequestErr();
	}

	auto id = [&object] () -> std::optional<rapidjson::Value> {
		auto it = object.FindMember("id");
		if (it == object.MemberEnd()) return {};
		rapidjson::Value & v = it->value;
		if (!v.IsString() && !v.IsNumber() && !v.IsNull()) _throwInvalidRequestErr();
		return std::move(v);
	}();

	auto method = [&object] () -> rapidjson::Value {
		const auto it = object.FindMember("method");
		if (it == object.MemberEnd()) _throwInvalidRequestErr();
		rapidjson::Value & v = it->value;
		if (!v.IsString()) _throwInvalidRequestErr();
		return std::move(v);
	}();

	auto params = [&object] () -> std::optional<rapidjson::Value> {
		const auto it = object.FindMember("params");
		if (it == object.MemberEnd()) return {};
		return std::move(it->value);
	}();

	std::optional<rapidjson::Document> requestDocument;
	if (requestDocumentPtr) requestDocument = std::move(*requestDocumentPtr);

	if (id) {
		BatchInfo * const batchInfo = getBatchInfo ? &getBatchInfo() : nullptr;

		std::unique_ptr<Request> request(new Request(*this, std::move(requestDocument),
				std::move(*id), std::move(method), std::move(params), batchInfo));

		if (batchInfo) {
			batchInfo->requests.push_back(BatchInfo::R{request.get(), {}});
			batchInfo->pendingCount++;
		}

		requests_.push_back(request.get());

		return request;
	} else {
		return std::unique_ptr<Call>(new Notification(*this, std::move(requestDocument),
				std::move(method), std::move(params)));
	}
}


JsonRpc::BatchInfo::R & JsonRpc::_getBatchRequest(Request & request) noexcept
{
	auto & response = std::get<Request::BatchResponse>(request.response_);
	const auto it = std::find_if(response.batchInfo->requests.begin(),
			response.batchInfo->requests.end(), [&request] (const BatchInfo::R & r) {
		return r.request == &request;
	});
	FLAT_ASSERT(it != response.batchInfo->requests.end());
	return *it;
}


const JsonRpc::BatchInfo::R & JsonRpc::_getBatchRequest(const Request & request) noexcept
{
	return _getBatchRequest(const_cast<Request&>(request));
}


void JsonRpc::_assignResponse(Request & request,
		const std::function<rapidjson::Value(rapidjson::Document & document)> makeValue) noexcept
{
	FLAT_ASSERT(!request.isAborted_);

	std::visit([&makeValue, &request] (auto && response) {
		using T = std::decay_t<decltype(response)>;

		if constexpr (std::is_same_v<T, Request::StandaloneResponse>) {
			FLAT_ASSERT(!response.document);
			response.document = rapidjson::Document();
			response.value = makeValue(*response.document);
		}

		if constexpr (std::is_same_v<T, Request::BatchResponse>) {
			rapidjson::Document & document = response.batchInfo->document;
			BatchInfo::R & r = _getBatchRequest(request);
			FLAT_ASSERT(!r.value);
			r.value = makeValue(document);
		}
	}, request.response_);
}


void JsonRpc::_requestResult(Request & request, const ResultCallback cb) noexcept
{
	auto makeValue = [&cb, &request, this] (rapidjson::Document & document) -> rapidjson::Value {
		ResultCallbackInfo info = cb(document.GetAllocator());
		auto value = _createMessageWithId(request.id_, document.GetAllocator());
		value.AddMember("result", std::move(info.result), document.GetAllocator());
		return value;
	};

	_assignResponse(request, makeValue);
}


void JsonRpc::_requestError(Request & request, const ErrorCallback cb) noexcept
{
	auto makeValue = [&cb, &request, this] (rapidjson::Document & document) -> rapidjson::Value {
		ErrorCallbackInfo info = cb(document.GetAllocator());
		return _createErrorResponse(request.id_, info.code, info.message,
				std::move(info.data), document.GetAllocator());
	};

	_assignResponse(request, makeValue);
}


void JsonRpc::_requestAbort(Request & request) noexcept
{
	FLAT_ASSERT(!request.isAborted_);
	FLAT_ASSERT(!request.isDone());
	request.isAborted_ = true;
}


void JsonRpc::_requestCommit(Request & request) noexcept
{
	if (!request.isAborted_) {
		std::visit([&request, this] (auto && response) {
			using T = std::decay_t<decltype(response)>;

			if constexpr (std::is_same_v<T, Request::StandaloneResponse>) {
				FLAT_ASSERT(response.value);
				_sendMessage(*response.value);
			}

			if constexpr (std::is_same_v<T, Request::BatchResponse>) {
				BatchInfo::R & r = _getBatchRequest(request);
				FLAT_ASSERT(r.value);
				FLAT_ASSERT(r.request == &request);
				r.request = nullptr;

				BatchInfo & batchInfo = *response.batchInfo;
				batchInfo.pendingCount--;
				if (batchInfo.pendingCount == 0) {
					rapidjson::Document & document = batchInfo.document;
					rapidjson::Value array(rapidjson::Type::kArrayType);
					for (auto & r : batchInfo.requests) {
						FLAT_ASSERT(!r.request);
						FLAT_ASSERT(r.value);
						array.PushBack(std::move(*r.value), document.GetAllocator());
					}
					document.SetArray() = std::move(array);
					_sendMessage(document);

					const auto batchIt = std::find_if(batchInfos_.begin(), batchInfos_.end(),
							[&batchInfo] (const auto & b) {
						return b.get() == &batchInfo;
					});
					FLAT_ASSERT(batchIt != batchInfos_.end());
					batchInfos_.erase(batchIt);
				}
			}
		}, request.response_);
	}

	const auto it = std::find_if(requests_.begin(), requests_.end(), [&request] (const auto & r) {
		return r == &request;
	});
	FLAT_ASSERT(it != requests_.end());
	requests_.erase(it);
}


void JsonRpc::_sendMessage(const rapidjson::Value & message) noexcept
{
	rapidjson::StringBuffer buffer;

	rapidjson::Writer w(buffer);
	const bool ok = message.Accept(w);
	FLAT_UNUSED_ASSERT(ok);

	//  2: colon+space delimiter
	// 10: max uint32 length size as string
	//  4: 4 bytes terminator \r\n\r\n
	//  1: zero string terminator for the sprintf
	static constexpr int kMaxHeaderSize = kContentLengthField.size() + 2 + 10 + 4 + 1;

	char headerBuffer[kMaxHeaderSize];
	const int headerSize = std::sprintf(headerBuffer, "%s: %d\r\n\r\n",
			kContentLengthField.data(), int(buffer.GetSize()));
	outputBuffer_.push(reinterpret_cast<const uint8_t*>(headerBuffer), headerSize);

	outputBuffer_.push(reinterpret_cast<const uint8_t*>(buffer.GetString()), buffer.GetSize());

	FLAT_DEBUG
			<< Flat::Debug::Log::raw(std::string_view(headerBuffer, headerSize))
			<< Flat::Debug::Log::raw(std::string_view(buffer.GetString(), buffer.GetSize()));
}


bool JsonRpc::_readLength(Reader & reader)
{
	FLAT_ASSERT(readingState_ == ReadingState::Length);

	// scan 4 bytes terminator
	static const uint8_t term[4] = {'\r', '\n', '\r', '\n'};

	while (true) {
		if (headerPos_ >= 4) {
			if (std::memcmp(headerBuffer_.data() + headerPos_ - 4, term, 4) == 0) break;
		}

		if (headerPos_ == kMaxHeaderSize) {
			throw Error("Invalid header size");
		}

		// append 1 byte and scan again
		if (int(headerBuffer_.size()) < headerPos_ + 1) {
			headerBuffer_.resize(headerPos_ + 1);
		}
		const int size = reader.read(headerBuffer_.data() + headerPos_, 1);
		if (size == 0) return false;
		headerPos_ += size;
	}

	const auto header = std::string_view(reinterpret_cast<const char*>(headerBuffer_.data()),
			headerPos_ - 2);

	int contentLength = -1;
	ContentType contentType = ContentType::Null;

	int pos = 0;

	while (pos != int(header.size())) {
		const auto breakPos = header.find("\r\n", pos);
		if (breakPos == std::string_view::npos) {
			throw Error("Invalid header format");
		}

		const auto line = header.substr(pos, breakPos - pos);
		pos = breakPos + 2;

		const auto delimiterPos = line.find(": ");
		if (delimiterPos == std::string_view::npos) {
			throw Error("Invalid header format");
		}

		const auto key = line.substr(0, delimiterPos);
		const auto value = line.substr(delimiterPos + 2);

		if (key == kContentLengthField) {
			if (contentLength != -1) {
				throw Error("Double Content-Length");
			}

			if (value.size() == 0 || value.size() > 10) {
				throw Error("Invalid Content-Length number");
			}

			char numberBuffer[11];
			std::memcpy(numberBuffer, value.data(), value.size());
			numberBuffer[value.size()] = '\0';

			char * end;
			const auto number = std::strtoll(numberBuffer, &end, 10);
			if (end != &numberBuffer[0] + value.size()) {
				throw Error("Failed parsing Content-Length number");
			}

			if (number < 0 || number > kMaxDataSize) {
				throw Error("Content-Length number is out of range");
			}

			contentLength = number;
		} else if (key == kContentTypeField) {
			if (contentType != ContentType::Null) {
				throw Error("Double Content-Type");
			}

			if (value == kJsonRpcMimeType) {
				contentType = ContentType::JsonRpc;
			} else {
				contentType = ContentType::Unknown;
			}
		} else {
			// ignore unknown field
		}
	}

	contentLength_ = contentLength;
	contentType_ = contentType;

	// +1 for the null terminator
	if (int(dataBuffer_.size()) < contentLength + 1) {
		dataBuffer_.resize(contentLength + 1);
	}
	dataPos_ = 0;
	dataBuffer_[contentLength] = '\0';

	readingState_ = ReadingState::Data;

	return true;
}


bool JsonRpc::_readData(Reader & reader)
{
	FLAT_ASSERT(readingState_ == ReadingState::Data);

	const int size = reader.read(dataBuffer_.data() + dataPos_, contentLength_ - dataPos_);
	dataPos_ += size;

	if (dataPos_ < contentLength_) return false;

	readingState_ = ReadingState::Length;
	headerPos_ = 0;

	return true;
}




}}
