
#pragma once

#include <string_view>




namespace Flat {
namespace Lsp {




enum class CallType {
	Notification,
	Request,
};




template <typename _Params, typename _Result>
struct CallInfo {
	typedef _Params Params;
	typedef _Result Result;

	inline static constexpr CallType type = std::is_same_v<Result, void> ?
			CallType::Notification : CallType::Request;

	const std::string_view name;
};




}}
