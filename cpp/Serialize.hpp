
#pragma once

#include <optional>

#include <rapidjson/document.h>




namespace Flat {
namespace Lsp {




struct ParseError {};

template <typename T>
T fromJson(const rapidjson::Value & v);

template <typename T>
rapidjson::Value toJson(const T & p, rapidjson::Document::AllocatorType & allocator);




}}
