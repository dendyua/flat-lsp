
#pragma once

#include "JsonRpc.hpp"




namespace Flat {
namespace Lsp {




inline static constexpr auto kReservedErrorCodeRangeStart = JsonRpc::ErrorCode(-32899);
inline static constexpr auto kReservedErrorCodeRangeEnd   = JsonRpc::ErrorCode(-32800);

inline static constexpr auto kErrorCodeContentModified  = JsonRpc::ErrorCode(-32801);
inline static constexpr auto kErrorCodeRequestCancelled = JsonRpc::ErrorCode(-32800);




}}
