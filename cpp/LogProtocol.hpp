
#pragma once

#include <Flat/Debug/Log.hpp>

#include "Types.hpp"




namespace Flat {
namespace Debug {




template <>
inline void Log::append<Lsp::Position>(
		const Lsp::Position & p) noexcept
{
	*this << "Position{" << NoSpaceOnce() << p.line << SpaceOnce() << p.character << NoSpaceOnce()
			<< "}";
}


template <>
inline void Log::append<Lsp::Range>(
		const Lsp::Range & r) noexcept
{
	*this << "Range{" << NoSpaceOnce() << r.start << SpaceOnce() << r.end << NoSpaceOnce() << "}";
}


template <>
inline void Log::append<Lsp::TextDocumentContentChangeEvent>(
		const Lsp::TextDocumentContentChangeEvent & e) noexcept
{
	*this << "TextDocumentContentChangeEvent{" << NoSpaceOnce()
			<< e.range << SpaceOnce()
			<< e.rangeLength << SpaceOnce()
			<< e.text
			<< NoSpaceOnce() << "}";
}




}}
