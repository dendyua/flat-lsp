
#pragma once

#include <any>
#include <cstddef>
#include <memory>
#include <optional>
#include <string>
#include <variant>
#include <vector>

#include "Call.hpp"




namespace Flat {
namespace Lsp {




struct CancelParams {
	std::variant<int, std::string> id;
};


struct Position {
	int line;
	int character;

	static Position null() noexcept { return Position{-1, -1}; }

	bool operator==(const Position & other) const noexcept {
		return line == other.line && character == other.character;
	}

	bool operator<(const Position & other) const noexcept {
		if (line < other.line) return true;
		if (line > other.line) return false;
		return character < other.character;
	}
};


struct Range {
	Position start;
	Position end;

	static Range null() noexcept { return Range{Position::null(), Position::null()}; }

	bool operator==(const Range & other) const noexcept {
		return start == other.start && end == other.end;
	}

	bool operator<(const Range & other) const noexcept {
		return start < other.start;
	}

	bool containsPosition(const Position & p) const noexcept {
		if (p.line < start.line) return false;
		if (p.line == start.line) {
			if (p.character < start.character) return false;
		}
		if (p.line > end.line) return false;
		if (p.line == end.line) {
			if (p.character > end.character) return false;
		}
		return true;
	}

	Flat::Lsp::Range shifted(int offset) const noexcept {
		return Flat::Lsp::Range{
			Flat::Lsp::Position{start.line + offset, start.character},
			Flat::Lsp::Position{end.line + offset, end.character},
		};
	}
};


struct Location {
	std::string uri;
	Range range;

	bool operator<(const Location & l) const noexcept {
		// TODO: Replace with C++20 operator <=>
		if (uri == l.uri) return range < l.range;
		return uri < l.uri;
	}
};


struct LocationLink {
	std::optional<Range> originSelectionRange;
	std::string targetUri;
	Range targetRange;
	Range targetSelectionRange;
};


enum class DiagnosticSeverity {
	Error       = 1,
	Warning     = 2,
	Information = 3,
	Hint        = 4,
};


enum class DiagnosticTag {
	Unnecessary = 1,
	Deprecated  = 2,
};


struct DiagnosticRelatedInformation {
	Location location;
	std::string message;
};


struct Diagnostic {
	Range range;
	std::optional<DiagnosticSeverity> severity;
	std::optional<std::variant<int, std::string>> code;
	std::optional<std::string> source;
	std::string message;
	std::optional<std::vector<DiagnosticTag>> tags;
	std::optional<std::vector<DiagnosticRelatedInformation>> relatedInformation;
};


enum class TextDocumentSyncKind {
	None        = 0,
	Full        = 1,
	Incremental = 2,
};


struct TextDocumentSyncOptions {
	std::optional<bool> openClose;
	std::optional<TextDocumentSyncKind> change;
};


struct WorkDoneProgressOptions {
	std::optional<bool> workDoneProgress;
};


struct CompletionOptions : WorkDoneProgressOptions {
	std::optional<std::vector<std::string>> triggerCharacters;
	std::optional<std::vector<std::string>> allCommitCharacters;
	std::optional<bool> resolveProvider;
};


struct HoverOptions : WorkDoneProgressOptions {};


struct SignatureHelpOptions : WorkDoneProgressOptions {
	std::optional<std::vector<std::string>> triggerCharacters;
	std::optional<std::vector<std::string>> retriggerCharacters;
};


struct DeclarationOptions : WorkDoneProgressOptions {
};


struct DocumentFilter {
	std::optional<std::string> language;

	std::optional<std::string> scheme;

	std::optional<std::string> pattern;
};


typedef std::vector<DocumentFilter> DocumentSelector;


struct TextDocumentRegistrationOptions {
	std::variant<DocumentSelector, std::nullptr_t> documentSelector;
};


struct StaticRegistrationOptions {
	std::optional<std::string> id;
};


struct DeclarationRegistrationOptions : DeclarationOptions, TextDocumentRegistrationOptions,
		StaticRegistrationOptions {
};


struct DefinitionOptions : WorkDoneProgressOptions {};


struct TypeDefinitionOptions : WorkDoneProgressOptions {};


struct TypeDefinitionRegistrationOptions : TextDocumentRegistrationOptions, TypeDefinitionOptions,
		StaticRegistrationOptions {
};


struct ImplementationOptions : WorkDoneProgressOptions {};


struct ImplementationRegistrationOptions : TextDocumentRegistrationOptions, ImplementationOptions,
		StaticRegistrationOptions {
};


struct ReferenceOptions : WorkDoneProgressOptions {};


struct DocumentHighlightOptions : WorkDoneProgressOptions {};


struct DocumentSymbolOptions : WorkDoneProgressOptions {};


enum class CodeActionKind {
	Empty,
	All,
	QuickFix,              // quickfix
	Refactor,              // refactor
	RefactorExtract,       // refactor.extract
	RefactorInline,        // refactor.inline
	RefactorRewrite,       // refactor.rewrite
	Source,                // source
	SourceOrganizeImports, // source.organizeImports
};


struct CodeActionOptions : WorkDoneProgressOptions {
	std::optional<std::vector<CodeActionKind>> codeActionKinds;
};


struct CodeLensOptions : WorkDoneProgressOptions {
	std::optional<bool> resolveProvider;
};


struct DocumentLinkOptions : WorkDoneProgressOptions {
	std::optional<bool> resolveProvider;
};


struct DocumentColorOptions : WorkDoneProgressOptions {};


struct DocumentColorRegistrationOptions : TextDocumentRegistrationOptions,
		StaticRegistrationOptions, DocumentColorOptions {
};


struct DocumentFormattingOptions : WorkDoneProgressOptions {};


struct DocumentRangeFormattingOptions : WorkDoneProgressOptions {};


struct DocumentOnTypeFormattingOptions {
	std::string firstTriggerCharacter;

	std::optional<std::vector<std::string>> moreTriggerCharacter;
};


struct RenameOptions : WorkDoneProgressOptions {
	std::optional<bool> prepareProvider;
};


struct FoldingRangeOptions : WorkDoneProgressOptions {};


struct FoldingRangeRegistrationOptions : TextDocumentRegistrationOptions, FoldingRangeOptions,
		StaticRegistrationOptions {
};


struct ExecuteCommandOptions : WorkDoneProgressOptions {
	std::vector<std::string> commands;
};


struct SelectionRangeOptions : WorkDoneProgressOptions {};


struct SelectionRangeRegistrationOptions : SelectionRangeOptions, TextDocumentRegistrationOptions,
		StaticRegistrationOptions {
};


struct WorkspaceSymbolOptions : WorkDoneProgressOptions {};


struct WorkspaceFoldersServerCapabilities {
	std::optional<bool> supported;

	std::optional<std::variant<std::string, bool>> changeNotifications;
};


enum class ResourceOperationKind {
	Create, // create
	Rename, // rename
	Delete, // delete
};


enum class FailureHandlingKind {
	Abort,                 // abort
	Transactional,         // transactional
	Undo,                  // undo
	TextOnlyTransactional, // textOnlyTransactional
};


struct WorkspaceEditClientCapabilities {
	std::optional<bool> documentChanges;

	std::optional<std::vector<ResourceOperationKind>> resourceOperations;

	std::optional<FailureHandlingKind> failureHandling;
};


struct DidChangeConfigurationClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct DidChangeWatchedFilesClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


enum class SymbolKind {
	File          = 1,
	Module        = 2,
	Namespace     = 3,
	Package       = 4,
	Class         = 5,
	Method        = 6,
	Property      = 7,
	Field         = 8,
	Constructor   = 9,
	Enum          = 10,
	Interface     = 11,
	Function      = 12,
	Variable      = 13,
	Constant      = 14,
	String        = 15,
	Number        = 16,
	Boolean       = 17,
	Array         = 18,
	Object        = 19,
	Key           = 20,
	Null          = 21,
	EnumMember    = 22,
	Struct        = 23,
	Event         = 24,
	Operator      = 25,
	TypeParameter = 26,
};


struct WorkspaceSymbolClientCapabilities {
	std::optional<bool> dynamicRegistration;

	struct SymbolKind {
		std::optional<std::vector<Lsp::SymbolKind>> valueSet;
	};
	std::optional<SymbolKind> symbolKind;
};


struct ExecuteCommandClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct TextDocumentSyncClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<bool> willSave;

	std::optional<bool> willSaveWaitUntil;

	std::optional<bool> didSave;
};


enum class MarkupKind {
	PlainText, // plaintext
	Markdown,  // markdown
};


enum class CompletionItemTag {
	Deprecated = 1,
};


enum class CompletionItemKind {
	Text          = 1,
	Method        = 2,
	Function      = 3,
	Constructor   = 4,
	Field         = 5,
	Variable      = 6,
	Class         = 7,
	Interface     = 8,
	Module        = 9,
	Property      = 10,
	Unit          = 11,
	Value         = 12,
	Enum          = 13,
	Keyword       = 14,
	Snippet       = 15,
	Color         = 16,
	File          = 17,
	Reference     = 18,
	Folder        = 19,
	EnumMember    = 20,
	Constant      = 21,
	Struct        = 22,
	Event         = 23,
	Operator      = 24,
	TypeParameter = 25,
};


struct CompletionClientCapabilities {
	std::optional<bool> dynamicRegistration;

	struct CompletionItem 	{
		std::optional<bool> snippetSupport;

		std::optional<bool> commitCharactersSupport;

		std::optional<std::vector<MarkupKind>> documentationFormat;

		std::optional<bool> deprecatedSupport;

		std::optional<bool> preselectSupport;

		struct TagSupport {
			std::vector<CompletionItemTag> valueSet;
		};
		std::optional<TagSupport> tagSupport;
	};
	std::optional<CompletionItem> completionItem;

	struct CompletionItemKind {
		std::optional<std::vector<Lsp::CompletionItemKind>> valueSet;
	};
	std::optional<CompletionItemKind> completionItemKind;

	std::optional<bool> contextSupport;
};


struct HoverClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<std::vector<MarkupKind>> contentFormat;
};


struct SignatureHelpClientCapabilities {
	std::optional<bool> dynamicRegistration;

	struct SignatureInformation {
		std::optional<std::vector<MarkupKind>> documentationFormat;

		struct ParameterInformation {
			std::optional<bool> labelOffsetSupport;
		};
		std::optional<ParameterInformation> parameterInformation;
	};
	std::optional<SignatureInformation> signatureInformation;

	std::optional<bool> contextSupport;
};


struct DeclarationClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<bool> linkSupport;
};


struct DefinitionClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<bool> linkSupport;
};


struct TypeDefinitionClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<bool> linkSupport;
};


struct ImplementationClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<bool> linkSupport;
};


struct ReferenceClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct DocumentHighlightClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct DocumentSymbolClientCapabilities {
	std::optional<bool> dynamicRegistration;

	struct SymbolKind {
		std::optional<std::vector<Lsp::SymbolKind>> valueSet;
	};
	std::optional<SymbolKind> symbolKind;

	std::optional<bool> hierarchicalDocumentSymbolSupport;
};


struct CodeActionClientCapabilities {
	std::optional<bool> dynamicRegistration;

	struct CodeActionLiteralSupport {
		struct CodeActionKind {
			std::vector<Lsp::CodeActionKind> valueSet;
		};
		CodeActionKind codeActionKind;
	};
	std::optional<CodeActionLiteralSupport> codeActionLiteralSupport;

	std::optional<bool> isPreferredSupport;
};


struct CodeLensClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct DocumentLinkClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<bool> tooltipSupport;
};


struct DocumentColorClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct DocumentFormattingClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct DocumentRangeFormattingClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct DocumentOnTypeFormattingClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct RenameClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<bool> prepareSupport;
};


struct PublishDiagnosticsClientCapabilities {
	std::optional<bool> relatedInformation;

	struct TagSupport {
		std::vector<DiagnosticTag> valueSet;
	};
	std::optional<TagSupport> tagSupport;

	std::optional<bool> versionSupport;
};


struct FoldingRangeClientCapabilities {
	std::optional<bool> dynamicRegistration;

	std::optional<int> rangeLimit;

	std::optional<bool> lineFoldingOnly;
};


struct SelectionRangeClientCapabilities {
	std::optional<bool> dynamicRegistration;
};


struct TextDocumentClientCapabilities {
	std::optional<TextDocumentSyncClientCapabilities> synchronization;

	std::optional<CompletionClientCapabilities> completion;

	std::optional<HoverClientCapabilities> hover;

	std::optional<SignatureHelpClientCapabilities> signatureHelp;

	std::optional<DeclarationClientCapabilities> declaration;

	std::optional<DefinitionClientCapabilities> definition;

	std::optional<TypeDefinitionClientCapabilities> typeDefinition;

	std::optional<ImplementationClientCapabilities> implementation;

	std::optional<ReferenceClientCapabilities> references;

	std::optional<DocumentHighlightClientCapabilities> documentHighlight;

	std::optional<DocumentSymbolClientCapabilities> documentSymbol;

	std::optional<CodeActionClientCapabilities> codeAction;

	std::optional<CodeLensClientCapabilities> codeLens;

	std::optional<DocumentLinkClientCapabilities> documentLink;

	std::optional<DocumentColorClientCapabilities> colorProvider;

	std::optional<DocumentFormattingClientCapabilities> formatting;

	std::optional<DocumentRangeFormattingClientCapabilities> rangeFormatting;

	std::optional<DocumentOnTypeFormattingClientCapabilities> onTypeFormatting;

	std::optional<RenameClientCapabilities> rename;

	std::optional<PublishDiagnosticsClientCapabilities> publishDiagnostics;

	std::optional<FoldingRangeClientCapabilities> foldingRange;

	std::optional<SelectionRangeClientCapabilities> selectionRange;
};


struct ClientCapabilities {
	struct Workspace {
		std::optional<bool> applyEdit;

		std::optional<WorkspaceEditClientCapabilities> workspaceEdit;

		std::optional<DidChangeConfigurationClientCapabilities> didChangeConfiguration;

		std::optional<DidChangeWatchedFilesClientCapabilities> didChangeWatchedFiles;

		std::optional<WorkspaceSymbolClientCapabilities> symbol;

		std::optional<ExecuteCommandClientCapabilities> executeCommand;

		std::optional<bool> workspaceFolders;

		std::optional<bool> configuration;
	};

	std::optional<Workspace> workspace;

	std::optional<TextDocumentClientCapabilities> textDocument;

	struct Window {
		std::optional<bool> workDoneProgress;
	};
	std::optional<Window> window;

	std::optional<std::any> experimental;
};


struct ServerCapabilities {
	std::optional<std::variant<TextDocumentSyncOptions, TextDocumentSyncKind>> textDocumentSync;

	std::optional<CompletionOptions> completionProvider;

	std::optional<std::variant<bool, HoverOptions>> hoverProvider;

	std::optional<SignatureHelpOptions> signatureHelpProvider;

	std::optional<std::variant<bool, DeclarationOptions, DeclarationRegistrationOptions>>
			declarationProvider;

	std::optional<std::variant<bool, DefinitionOptions>> definitionProvider;

	std::optional<std::variant<bool, TypeDefinitionOptions, TypeDefinitionRegistrationOptions>>
			typeDefinitionProvider;

	std::optional<std::variant<bool, ImplementationOptions, ImplementationRegistrationOptions>>
			implementationProvider;

	std::optional<std::variant<bool, ReferenceOptions>> referencesProvider;

	std::optional<std::variant<bool, DocumentHighlightOptions>> documentHighlightProvider;

	std::optional<std::variant<bool, DocumentSymbolOptions>> documentSymbolProvider;

	std::optional<std::variant<bool, CodeActionOptions>> codeActionProvider;

	std::optional<CodeLensOptions> codeLensProvider;

	std::optional<DocumentLinkOptions> documentLinkProvider;

	std::optional<std::variant<bool, DocumentColorOptions, DocumentColorRegistrationOptions>>
			colorProvider;

	std::optional<std::variant<bool, DocumentFormattingOptions>> documentFormattingProvider;

	std::optional<std::variant<bool, DocumentRangeFormattingOptions>>
			documentRangeFormattingProvider;

	std::optional<DocumentOnTypeFormattingOptions> documentOnTypeFormattingProvider;

	std::optional<std::variant<bool, RenameOptions>> renameProvider;

	std::optional<std::variant<bool, FoldingRangeOptions, FoldingRangeRegistrationOptions>>
			foldingRangeProvider;

	std::optional<ExecuteCommandOptions> executeCommandProvider;

	std::optional<std::variant<bool, SelectionRangeOptions, SelectionRangeRegistrationOptions>>
			selectionRangeProvider;

	std::optional<std::variant<bool, WorkspaceSymbolOptions>> workspaceSymbolProvider;

	struct Workspace {
		std::optional<WorkspaceFoldersServerCapabilities> workspaceFolders;
	};
	std::optional<Workspace> workspace;

	std::optional<std::any> experimental;
};


typedef std::variant<int, std::string> ProgressToken;

//struct ProgressParams
//{
//	ProgressToken token;

//	std::variant<> value;
//};


struct WorkDoneProgressParams {
	std::optional<ProgressToken> workDoneToken;
};


enum class Trace {
	Off,      // off
	Messages, // messages
	Verbose,  // verbose
};


struct WorkspaceFolder {
	std::string uri;
	std::string name;
};


struct InitializeParams : WorkDoneProgressParams {
	std::variant<int, std::nullptr_t> processId;

	struct ClientInfo {
		std::string name;
		std::optional<std::string> version;
	};
	std::optional<ClientInfo> clientInfo;

	std::optional<std::variant<std::string, std::nullptr_t>> rootPath;

	std::variant<std::string, std::nullptr_t> rootUri;

	std::optional<std::any> initializationOptions;

	ClientCapabilities capabilities;

	std::optional<Trace> trace;

	std::optional<std::variant<std::vector<WorkspaceFolder>, std::nullptr_t>> workspaceFolders;
};


struct InitializeResult {
	ServerCapabilities capabilities;

	struct ServerInfo {
		std::string name;
		std::optional<std::string> version;
	};
	std::optional<ServerInfo> serverInfo;
};


struct InitializedParams {};



struct TextDocumentIdentifier {
	std::string uri;
};


struct TextDocumentItem {
	std::string uri;
	std::string languageId;
	int version;
	std::string text;
};


struct VersionedTextDocumentIdentifier : TextDocumentIdentifier {
	std::variant<int, std::nullptr_t> version;
};


struct TextDocumentContentChangeEvent {
	std::optional<Range> range;

	std::optional<int> rangeLength;

	std::string text;
};


struct DidOpenTextDocumentParams {
	TextDocumentItem textDocument;
};


struct DidCloseTextDocumentParams {
	TextDocumentIdentifier textDocument;
};


struct DidChangeTextDocumentParams {
	VersionedTextDocumentIdentifier textDocument;

	std::vector<TextDocumentContentChangeEvent> contentChanges;
};


struct DidSaveTextDocumentParams {
	TextDocumentIdentifier textDocument;
	std::optional<std::string> text;
};



struct PartialResultParams {
	std::optional<ProgressToken> partialResultToken;
};


struct DocumentSymbolParams : WorkDoneProgressParams, PartialResultParams {
	TextDocumentIdentifier textDocument;
};


struct DocumentSymbol {
	std::string name;

	std::optional<std::string> detail;

	SymbolKind kind;

	std::optional<bool> deprecated;

	Range range;

	Range selectionRange;

	std::optional<std::vector<DocumentSymbol>> children;
};


struct SymbolInformation {
	std::string name;

	SymbolKind kind;

	std::optional<bool> deprecated;

	Location location;

	std::optional<std::string> containerName;
};


typedef std::variant<std::vector<DocumentSymbol>, std::vector<SymbolInformation>, std::nullptr_t>
		DocumentSymbolResult;


struct TextDocumentPositionParams {
	TextDocumentIdentifier textDocument;
	Position position;
};


struct DocumentHighlightParams : TextDocumentPositionParams, WorkDoneProgressParams,
		PartialResultParams {
};


enum class DocumentHighlightKind {
	Text  = 1,
	Read  = 2,
	Write = 3,
};


struct DocumentHighlight {
	Range range;
	std::optional<DocumentHighlightKind> kind;
};


typedef std::variant<std::vector<DocumentHighlight>, std::nullptr_t> DocumentHighlightResult;


struct ReferenceContext {
	bool includeDeclaration;
};


struct ReferenceParams : TextDocumentPositionParams, WorkDoneProgressParams, PartialResultParams {
	ReferenceContext context;
};


typedef std::variant<std::vector<Location>, std::nullptr_t> ReferencesResult;


struct DefinitionParams : TextDocumentPositionParams, WorkDoneProgressParams, PartialResultParams {
};


typedef std::variant<Location, std::vector<Location>, std::vector<LocationLink>, std::nullptr_t>
		DefinitionResult;


struct PublishDiagnosticsParams {
	std::string uri;
	std::optional<int> version;
	std::vector<Diagnostic> diagnostics;
};




static constexpr CallInfo<CancelParams, void> kCancelNotification{"$/cancelRequest"};

static constexpr CallInfo<InitializeParams,  InitializeResult> kInitializeRequest      {"initialize"};
static constexpr CallInfo<InitializedParams, void>             kInitializedNotification{"initialized"};
static constexpr CallInfo<void,              std::nullptr_t>   kShutdownRequest        {"shutdown"};
static constexpr CallInfo<void,              void>             kExitNotification       {"exit"};

static constexpr CallInfo<DidOpenTextDocumentParams,   void> kTextDocumentDidOpenNotification  {"textDocument/didOpen"};
static constexpr CallInfo<DidCloseTextDocumentParams,  void> kTextDocumentDidCloseNotification {"textDocument/didClose"};
static constexpr CallInfo<DidChangeTextDocumentParams, void> kTextDocumentDidChangeNotification{"textDocument/didChange"};

static constexpr CallInfo<void,                    std::nullptr_t>          kTextDocumentCompletionRequest       {"textDocument/completion"}; // FIXME
static constexpr CallInfo<DocumentSymbolParams,    DocumentSymbolResult>    kTextDocumentDocumentSymbolRequest   {"textDocument/documentSymbol"};
static constexpr CallInfo<DocumentHighlightParams, DocumentHighlightResult> kTextDocumentDocumentHighlightRequest{"textDocument/documentHighlight"};
static constexpr CallInfo<ReferenceParams,         ReferencesResult>        kTextDocumentReferencesRequest       {"textDocument/references"};
static constexpr CallInfo<DefinitionParams,        DefinitionResult>        kTextDocumentDefinitionRequest       {"textDocument/definition"};




}}
