
#pragma once

#include <memory>
#include <optional>
#include <queue>
#include <stdexcept>
#include <string>
#include <variant>
#include <vector>

#include <rapidjson/document.h>

#include <Flat/Utils/RingBuffer.hpp>

#include "Call.hpp"




namespace Flat {
namespace Lsp {




class FLAT_LSP_EXPORT JsonRpc {
public:
	enum class ErrorCode {
		ParseError     = -32700, // Invalid JSON was received by the server.
		                         // An error occurred on the server while parsing the JSON text.
		InvalidRequest = -32600, // The JSON sent is not a valid Request object.
		MethodNotFound = -32601, // The method does not exist / is not available.
		InvalidParams  = -32602, // Invalid method parameter(s).
		InternalError  = -32603, // Internal JSON-RPC error.
	};

	inline static constexpr auto kReservedErrorCodeRangeStart = ErrorCode(-32099);
	inline static constexpr auto kReservedErrorCodeRangeEnd   = ErrorCode(-32000);

	struct Error : public std::runtime_error {
		using std::runtime_error::runtime_error;
	};

	struct CallError {
		ErrorCode code;
		std::string message;
		std::optional<rapidjson::Value> data;
	};

	struct Reader {
		virtual int read(uint8_t * data, int size) = 0;
	};

	struct Writer {
		virtual int write(const uint8_t * data, int size) noexcept = 0;
	};

	struct ResultCallbackInfo {
		rapidjson::Value result;
	};

	struct ErrorCallbackInfo {
		ErrorCode code;
		std::string message;
		std::optional<rapidjson::Value> data;
	};

	struct NotificationCallbackInfo {
		std::string method;
		rapidjson::Value params;
	};

	typedef std::function<ResultCallbackInfo(rapidjson::Document::AllocatorType & allocator)>
			ResultCallback;
	typedef std::function<ErrorCallbackInfo(rapidjson::Document::AllocatorType & allocator)>
			ErrorCallback;
	typedef std::function<NotificationCallbackInfo(rapidjson::Document::AllocatorType & allocator)>
			NotificationCallback;

	class Request;

	struct BatchInfo {
		struct R {
			Request * request;
			std::optional<rapidjson::Value> value;
		};

		rapidjson::Document document{};
		int pendingCount = 0;
		std::vector<R> requests{};
	};

	class Call {
	public:
		CallType type() const noexcept { return type_; }
		std::string_view method() const noexcept;
		const std::optional<rapidjson::Value> & params() const noexcept { return params_; }

	private:
		Call(JsonRpc & jsonRpc, CallType type, std::optional<rapidjson::Document> requestDocument,
				 rapidjson::Value method, std::optional<rapidjson::Value> params);

		JsonRpc & jsonRpc_;
		const CallType type_;
		const std::optional<rapidjson::Document> requestDocument_;
		rapidjson::Value method_;
		std::optional<rapidjson::Value> params_;

		// TODO: Debugging only. To be removed.
		std::string methodString_;

		friend class JsonRpc;
	};

	class Notification : public Call {
	private:
		Notification(JsonRpc & jsonRpc, std::optional<rapidjson::Document> requestDocument,
				 rapidjson::Value method, std::optional<rapidjson::Value> params);

		friend class JsonRpc;
	};

	class Request : public Call {
	public:
		~Request() noexcept { jsonRpc_._requestCommit(*this); }

		const rapidjson::Value & id() const noexcept { return id_; }

		bool isDone() const noexcept;

		void result(ResultCallback cb) noexcept { jsonRpc_._requestResult(*this, cb); }
		void error(ErrorCallback cb) noexcept { jsonRpc_._requestError(*this, cb); }
		void abort() noexcept { jsonRpc_._requestAbort(*this); }

	private:
		struct StandaloneResponse {
			std::optional<rapidjson::Document> document{};
			std::optional<rapidjson::Value> value{};
		};

		struct BatchResponse {
			BatchInfo * batchInfo;
		};

		typedef std::variant<StandaloneResponse, BatchResponse> Response;

		Request(JsonRpc & jsonRpc, std::optional<rapidjson::Document> requestDocument,
				rapidjson::Value id, rapidjson::Value method,
				std::optional<rapidjson::Value> params, BatchInfo * batchInfo);

		rapidjson::Value id_;
		Response response_;
		bool isAborted_ = false;

		// TODO: Debugging only. To be removed.
		std::string idString_;

		friend class JsonRpc;
	};

	struct Callbacks {
		virtual void notification(Notification & notification) noexcept = 0;
		virtual void request(std::unique_ptr<Request> request) noexcept = 0;
	};

	static std::string_view jsonStringToStringView(const rapidjson::Value & v) noexcept;
	static std::string jsonValueToString(const rapidjson::Value & v) noexcept;

	JsonRpc(Callbacks & callbacks);
	~JsonRpc();

	void read(Reader & reader);
	void write(Writer & writer);

	void sendNotification(NotificationCallback cb);

private:
	enum class ReadingState {
		Length,
		Data,
	};

	enum class ContentType {
		Null,
		JsonRpc,
		Unknown,
	};

	typedef std::function<BatchInfo &()> GetBatchInfo;

	static CallError _throwInvalidRequestErr();

	template <typename Allocator>
	rapidjson::Value _createMessage(Allocator & allocator) const noexcept;

	template <typename Allocator>
	rapidjson::Value _createMessageWithId(const rapidjson::Value & id,
			Allocator & allocator) const noexcept;

	template <typename Allocator>
	rapidjson::Value _createErrorResponse(const rapidjson::Value & id, ErrorCode code,
			std::string_view message, std::optional<rapidjson::Value> data,
			Allocator & allocator) const noexcept;

	std::unique_ptr<Call> _createCall(rapidjson::Document * requestDocumentPtr,
			rapidjson::Value & value, GetBatchInfo getBatchInfo);
	void _processRequest(std::unique_ptr<Request> request);

	static BatchInfo::R & _getBatchRequest(Request & request) noexcept;
	static const BatchInfo::R & _getBatchRequest(const Request & request) noexcept;
	void _assignResponse(Request & request,
			std::function<rapidjson::Value(rapidjson::Document & document)> makeValue) noexcept;
	void _requestResult(Request & request, ResultCallback cb) noexcept;
	void _requestError(Request & request, ErrorCallback cb) noexcept;
	void _requestAbort(Request & request) noexcept;
	void _requestCommit(Request & request) noexcept;

	void _sendMessage(const rapidjson::Value & message) noexcept;

	bool _readLength(Reader & reader);
	bool _readData(Reader & reader);

	Callbacks & callbacks_;

	ReadingState readingState_ = ReadingState::Length;

	std::vector<uint8_t> headerBuffer_;
	int headerPos_ = 0;
	int contentLength_;
	ContentType contentType_;

	std::vector<uint8_t> dataBuffer_;
	int dataPos_;

	Flat::Utils::RingBuffer outputBuffer_;

	std::vector<Request*> requests_;
	std::vector<std::unique_ptr<BatchInfo>> batchInfos_;
};




}}
